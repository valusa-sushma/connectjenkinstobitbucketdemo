﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReadInputandRetryToShow.aspx.cs" Inherits="ConnectJenkinsToBitBucketDemo.ReadInputandRetryToShow" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="UsernameLbl" runat="server" Text="User name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="UserNametxt" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="PasswordLbl" runat="server" Text="Password"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="Passwordtxt" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="SubmitBtn" runat="server" OnClick="SubmitBtn_Click" style="height: 26px" Text="Submit" />
                </td>
                <td>
                    <asp:Label ID="SuccessLbl" runat="server"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
