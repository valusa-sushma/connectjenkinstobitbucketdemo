﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.TransientFaultHandling;
using System.Net;

namespace ConnectJenkinsToBitBucketDemo
{
    public partial class ReadInputandRetryToShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SubmitBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string uName = UserNametxt.Text;
                SuccessLbl.Text = string.Format("{0} login is successful .You can login to main page.Added nuget package.Pushing the changes. ", uName);
                var retryStrategy = new Incremental(2, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));

                // Step 2 
                var retryPolicy = new RetryPolicy<CustomTransientErrorDetectionStrategy>(retryStrategy);

                // Step 3
                retryPolicy.ExecuteAction(NavigateTo);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

// This is the code that may experience transient errors
static private void NavigateTo()
{
    Console.WriteLine(DateTime.Now);

    WebClient wc = new WebClient();
    wc.DownloadString("c:\\temp.txt");
}
        
    }
    internal class CustomTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
    {
        public bool IsTransient(Exception ex)
        {
            if (ex is WebException)
                return true;
            return false;
        }
    }
}